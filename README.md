Liens vers notre diapo : https://drive.google.com/open?id=1AiqWwfgNDy-e6EhezEhLRiICYyxc2BuilAIZRW5J8KA

Documentation Play 2.6 (faire attention a la version): https://www.playframework.com/documentation/2.6.x/Home

Documentation akka: https://doc.akka.io/docs/akka/2.5/index.html

Documentation Scala: https://docs.scala-lang.org/

#Installation :
Ouvrez le projet dans IntelliJ
Installez le plugin Scala (File>Settings>Plugins)
Lancez la commande >sbt run
Ouvrez localhost:9000 dans un navigateur

Si vous ne parvenez pas a faire fonctionner sbt ou que la version disponnible n est pas compatible:
enlevez tous les prefixes "no-" devans les fichiers et dossiers gradle et lancez la commande gradle runPlayBinary via IntelliJ


Tout le travail que vous aurez à faire est facilement repérable grâce aux marqueurs “TODO” (navigation facile dans IntelliJ)


#Partie 1
Dans cette partie vous verrez comment fonctionne le moteur de template Twirl, le router et les assets.

L'objectif de cette partie est d'afficher une liste de produits, le css est deja gere dans le fichier public/stylesheets/main.css 

* Implémentez Partie1Controller de sorte que la methode listeProduits retourne le template partie1 en lui fournissant les bons parametres
* Implémentez Partie1.scala.html, vous devez iterer sur la liste de produit et afficher leurs infos :  @for(item <- list){ @item }
* Voir le résultat localhost:9000/partie1
* Vous devez ajouter le liens vers la partie 2 en utilisant le reverse Router pour passer à la seconde partie

#Partie2
Dans cette partie vous apprendrez à créer un Controller simple qui appellera un acteur
A partir de maintenant vous n’avez plus besoin d'implémenter les templates, nous les avons créés pour vous.

Nous avons changé la méthode de chargement des produits,
Désormais le front fera une série d’appels sur la methode getProduit du controller Partie1Controller en spécifiant l’id du produit
Vous devez dans cette methode appeller le service ServiceSynchrone qui lui même appellera les différentes ressources pour construire le produit à retourner
Les ressources mettent volontairement du temps à répondre pour mettre en évidence les avantages de play.


    
Dans le controller:
* Utiliser Await.result(appelActeur, duree), pour faire un  appel synchrone
* Pour faire un appel a un acteur utiliser ” ?” ex: (service? param).mapTo[TypeAttendu]
* Await.result prend en dernier paramètre la durée d’attente maximale
Dans le ActeurService2, appelez les différents dao pour récupérer les infos du produit

? : est utilise pour appeller un acteur et attendre son resultat sous forme de Future

! : est utilise pour envoyer un message a un acteur sans attendre de reponse, il sera utilise principalement pour repondre a l appellant

Dans ActeurDaoDescription:
* Vous pouvez vous inspirer des autres dao, sans copier coller bien sur
* Appelez la ressource externe pour récupérer le la description
* retournez le résultat à l'appelant



#Partie 3
Implémentez: Partie3Controller ActeurServicePartie3 ActeurDaoDescriptionAsync

Dans cette partie vous apprendrez à manipuler un contrôleur et un acteur asynchrone ainsi qu’à questionner des ressources parallèlement.

Vous devez maintenant implémenter  le controller et le service Asynchrone qui effectuera le même travail que ceux de la partie précédente mais de manière asynchrone

Regardez  ActeurDaoNomAsync et ActeurDaoPrixAsync pour vous aider à écrire ActeurDaoDescriptionAsync

Vous pouvez cocher le boutton radio "asynchrone" sur la page localhost:9000/pageListeProduits pour observer le rendu


#Observations finales
Vous pouvez constater le gain de performance par une programmation asynchrone dans la derniere page web.

Pour comparer les performances: localhost:9000/pageTestPerf
